import 'package:flutter/material.dart';
import 'package:listas_sabado/pages/home_page.dart';
import 'package:listas_sabado/pages/lista_page.dart';
import 'package:listas_sabado/pages/lista_users_map_page.dart';
import 'package:listas_sabado/pages/lista_users_page.dart';
import 'package:listas_sabado/pages/card_page.dart';

Map<String, WidgetBuilder> getRoutes() {
  return <String, WidgetBuilder>{
    '/': (context) => HomePage(),
    'lista_simple': (context) => ListaPage(),
    'lista_simple_user': (context) => ListaUsersPage(),
    'lista_simple_user_map': (context) => ListaUsersMapPage(),
    'cards': (context) => CardPage(),
  };
}
