import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:listas_sabado/pages/lista_page.dart';
import 'package:listas_sabado/routes/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //home: Lista(),
      //home: HomePage(),
      initialRoute: '/',
      routes: getRoutes(),
      onGenerateRoute: (RouteSettings settings) {
        developer.log('Ruta seleccionada: ${settings.name}');
        return MaterialPageRoute(builder: (context)=>ListaPage());
      },
    );
  }
}
