import 'package:flutter/material.dart';
import 'package:listas_sabado/data/users.dart';

class ListaUsersPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Icon(Icons.format_list_bulleted),
            SizedBox(
              width: 10.0,
            ),
            Text('Lista simple users')
          ],
        ),
      ),
      body: ListView(
        children: _listaSimpleUsers(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.home),
        onPressed: () {
          Navigator.pushNamed(context, '/');
        },
      ),
    );
  }

  List<Widget> _listaSimpleUsers() {
    List<Widget> lista = List<Widget>();
    for (var user in getUsers()) {
      lista
        ..add(ListTile(
          leading: Icon(Icons.perm_identity),
          title: Text(user['nombre']),
          subtitle: Text(user['ciudad']),
          trailing: Icon(Icons.arrow_forward_ios),
          onTap: () {},
        ))
        ..add(Divider(
          thickness: 2.0,
        ));
    }
    return lista;
  }
}
