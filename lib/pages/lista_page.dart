import 'package:flutter/material.dart';
import 'package:listas_sabado/data/users.dart';

class ListaPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            children: <Widget>[
              Icon(Icons.format_list_bulleted),
              SizedBox(
                width: 10.0,
              ),
              Text('Lista simple')
            ],
          ),
        ),
        body: ListView(
          children: _listaSimple(),
        ));
  }

  List<Widget> _listaSimple() {
    List<Widget> lista = List<Widget>();
    for (int i = 0; i < 20; i++) {
      lista.add(ListTile(
        leading: Icon(Icons.perm_identity),
        title: Text('Nombre'),
        subtitle: Text('Ciudad'),
        trailing: Icon(Icons.arrow_forward_ios),
        onTap: () {},
      ));
      lista.add(Divider(
        thickness: 2.0,
      ));
    }
    return lista;
  }
}
