import 'package:flutter/material.dart';
import 'package:listas_sabado/data/users.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tarjetas'),
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: _listaCards(),
      ),
    );
  }

  List<Widget> _listaCards() {
    List<Widget> lista = List<Widget>();
    for (var user in getUsers()) {
      lista
        ..add(_card(user))
        ..add(Divider(
          thickness: 2.0,
        ));
    }
    return lista;
  }

  Widget _card(var user) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Image(
            image: NetworkImage(
                'https://www.pinclipart.com/picdir/middle/355-3553881_stockvader-predicted-adig-user-profile-icon-png-clipart.png'),
          ),
          ListTile(
            leading: Icon(Icons.perm_identity),
            title: Text(user['nombre']),
            subtitle: Text(user['ciudad']),
          ),
          ButtonTheme.bar(
            child: ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: const Text('BUY TICKETS'),
                  onPressed: () {
                    /* ... */
                  },
                ),
                FlatButton(
                  child: const Text('LISTEN'),
                  onPressed: () {
                    /* ... */
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
