import 'package:flutter/material.dart';
import 'package:listas_sabado/data/users.dart';
import 'package:listas_sabado/pages/home_page.dart';

import 'lista_page.dart';

class ListaUsersMapPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            children: <Widget>[
              Icon(Icons.format_list_bulleted),
              SizedBox(
                width: 10.0,
              ),
              Text('Lista con Map')
            ],
          ),
        ),
        body: ListView(
          children: _listaMapUsers(context),
        ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: (){
          Navigator.pop(context);
        },
      ),
    );
  }

  List<Widget> _listaMapUsers(BuildContext context) {
    return getUsers().map((user) {
      return Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.perm_identity),
            title: Text(user['nombre']),
            subtitle: Text(user['ciudad']),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){
              Navigator.pushNamed(context, 'user_details', arguments: 'user');
            },
          ),
          Divider(
            thickness: 2.0,
            color: Colors.lightBlue,
            indent: 70.0,
            endIndent: 20.0,
          )
        ],
      );
    }).toList();
  }
}
