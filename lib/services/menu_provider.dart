import 'dart:convert';

import 'package:flutter/services.dart';

class _MenuProvider {
  List<dynamic> menu = [];

  _MenuProvider() {}

  Future<List<dynamic>> loadData() async {
    final resp = await rootBundle.loadString('static/menu/menu.json');

    Map dataMap = json.decode(resp);
    menu = dataMap['routes'];

    return menu;
  }
}

final menuProvider = _MenuProvider();
